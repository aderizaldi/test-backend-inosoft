# Test Backend Inosoft

Rest API penjualan kendaraan

## Instalasi Project Laravel

Gunakan [git](https://git-scm.com/) untuk cloning project ini:

```bash
git clone https://gitlab.com/aderizaldi/simple-rest-api-laravel-mongodb.git
```

atau jika menggunakan ssh:

```bash
git clone git@gitlab.com:aderizaldi/simple-rest-api-laravel-mongodb.git
```

Selanjutnya jika proses cloning sudah selesai, rename file **.env.example** menjadi **.env** dan sesuaikan configurasi yang ada.

Setelah itu Lakukan instalasi vendor menggunakan [composer](https://getcomposer.org/)

```bash
composer install
```

## Import Database

Buat database MongoDB baru lalu import collection yang terletak pada folder "collections".

Selain menggunakan import, dapat juga menggunakan seeders dengan menggunakan command:

```bash
php artisan db:seed
```

## Run REST API dan Testing

Untuk menjalankan project menggunakan command:

```bash
php artisan serve
```

Selanjutnya REST API sudah dapat digunakan dan untuk penggunaan dan contoh response dapat dilihat pada dokumentasi berikut:

+[Dokumentasi](https://documenter.getpostman.com/view/19571378/2s8Yevpq2q)

Selain itu juga dapat melakukan testing menggunakan unit testing menggunakan command:

```bash
composer unit-test
```
