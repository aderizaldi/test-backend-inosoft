<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Auth')->group(function () {
    Route::post('register', 'RegisterController');
    Route::post('login', 'LoginController');
    Route::post('logout', 'LogoutController');
});

Route::get('kendaraan/motor', 'KendaraanController@allMotor');
Route::get('kendaraan/mobil', 'KendaraanController@allMobil');
Route::post('kendaraan/{kendaraan}/buy', 'KendaraanController@buyKendaraan');
Route::resource('kendaraan', "KendaraanController")->only([
    'index', 'store', 'show', 'update', 'destroy'
]);
