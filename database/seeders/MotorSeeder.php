<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Kendaraan;
use Faker\Factory as Faker;

class MotorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for ($i = 0; $i < 75; $i++) {
            Kendaraan::create([
                'tahun_keluaran' => $faker->year(),
                'warna' => $faker->colorName(),
                'harga' => $faker->numberBetween(5000000, 10000000000),
                'motor' => [
                    'mesin' => $faker->word,
                    'tipe_suspensi' => $faker->word,
                    'tipe_transmisi' => $faker->randomElement(array('Manual', 'Matic')),
                ],
                'stok' => $faker->boolean(85),
            ]);
        }
    }
}
