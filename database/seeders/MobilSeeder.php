<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Kendaraan;
use Faker\Factory as Faker;

class MobilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for ($i = 0; $i < 75; $i++) {
            Kendaraan::create([
                'tahun_keluaran' => $faker->year(),
                'warna' => $faker->colorName(),
                'harga' => $faker->numberBetween(5000000, 10000000000),
                'mobil' => [
                    'mesin' => $faker->word,
                    'kapasitas_penumpang' => $faker->numberBetween(2, 12),
                    'tipe' => $faker->word,
                ],
                'stok' => $faker->boolean(85),
            ]);
        }
    }
}
