<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for ($i = 0; $i < 25; $i++) {
            $name = $faker->name;
            $username = strtolower(preg_replace("/[^A-Za-z0-9]/", '', $name)) . $faker->randomNumber(5, true);
            $username = str_replace(" ", "", $username);
            User::create([
                'name' => $name,
                'username' => $username,
                'email' => $faker->email,
                'password' => Hash::make('password'),
            ]);
        }
    }
}
