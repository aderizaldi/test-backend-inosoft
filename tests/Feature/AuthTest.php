<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class AuthTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testFailedRegistration()
    {
        $this->json('POST', 'api/register', ['Accept' => 'application/json'])
            ->assertStatus(400);
    }


    public function testSuccessRegistration()
    {
        $userData = [
            "name" => "SampelUnique270501",
            "username" => "sampel123",
            "email" => "sampel123@test.com",
            "password" => "sampel123",
        ];

        $this->json('POST', 'api/register', $userData, ['Accept' => 'application/json'])
            ->assertStatus(200);

        User::where('username', 'sampel123')->delete();
    }

    public function testFailedLogin()
    {
        $this->json('POST', 'api/login')
            ->assertStatus(400);
    }

    public function testSuccessLogin()
    {
        User::create([
            "name" => "SampelUnique270501",
            "username" => "sampel123",
            "email" => "sampel123@test.com",
            "password" => bcrypt("sampel123"),
        ]);


        $loginData = ['username' => 'sampel123', 'password' => 'sampel123'];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200);

        $this->assertAuthenticated();

        User::where('username', 'sampel123')->delete();
    }
}
