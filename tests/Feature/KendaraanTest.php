<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Kendaraan;
use Illuminate\Testing\TestResponse;

class KendaraanTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAuthenticated()
    {
        $this->json('get', 'api/kendaraan', [], ['Accept' => 'application/json'])
            ->assertStatus(401);
        $this->json('get', 'api/kendaraan/_id', [], ['Accept' => 'application/json'])
            ->assertStatus(401);
        $this->json('get', 'api/kendaraan/mobil', [], ['Accept' => 'application/json'])
            ->assertStatus(401);
        $this->json('get', 'api/kendaraan/motor', [], ['Accept' => 'application/json'])
            ->assertStatus(401);
        $this->json('post', 'api/kendaraan/', [],  ['Accept' => 'application/json'])
            ->assertStatus(401);
        $this->json('put', 'api/kendaraan/_id', [], ['Accept' => 'application/json'])
            ->assertStatus(401);
        $this->json('delete', 'api/kendaraan/_id', [], ['Accept' => 'application/json'])
            ->assertStatus(401);
    }

    public function testGetKendaraan()
    {
        //Login
        User::create([
            "name" => "SampelUnique270501",
            "username" => "sampel123",
            "email" => "sampel123@test.com",
            "password" => bcrypt("sampel123"),
        ]);


        $loginData = ['username' => 'sampel123', 'password' => 'sampel123'];

        $data = $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])->json('data');
        $token = $data['token'];


        $kendaraan = Kendaraan::create([
            [
                'tahun_keluaran' => 2002,
                'warna' => "rahasia270501",
                'harga' => 200000000,
                'mobil' => [
                    'mesin' => "Dino",
                    'kapasitas_penumpang' => 4,
                    'tipe' => "Flamming",
                ],
                'stok' => true,
            ],
            // [
            //     'tahun_keluaran' => 2002,
            //     'warna' => "rahasia270501",
            //     'harga' => 1500000,
            //     'motor' => [
            //         'mesin' => "Rex",
            //         'tipe_suspensi' => "Sunny",
            //         'tipe_transmisi' => "Matic",
            //     ],
            //     'stok' => true,
            // ]
        ]);
        $id = $kendaraan->id;

        $this->json('get', 'api/kendaraan', [], ['Accept' => 'application/json', 'Authorization' => "Bearer " .  $token,])->assertStatus(200);

        $this->json('get', 'api/kendaraan/mobil', [], ['Accept' => 'application/json', 'Authorization' => "Bearer " .  $token,])->assertStatus(200);

        $this->json('get', 'api/kendaraan/motor', [], ['Accept' => 'application/json', 'Authorization' => "Bearer " .  $token,])->assertStatus(200);

        $this->json('get', 'api/kendaraan/' . $id, [], ['Accept' => 'application/json', 'Authorization' => "Bearer " .  $token,])->assertStatus(200);

        Kendaraan::find($id)->delete();
        User::where('username', 'sampel123')->delete();
    }

    public function testFailedCreateKendaraan()
    {
        User::create([
            "name" => "SampelUnique270501",
            "username" => "sampel123",
            "email" => "sampel123@test.com",
            "password" => bcrypt("sampel123"),
        ]);


        $loginData = ['username' => 'sampel123', 'password' => 'sampel123'];

        $data = $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])->json('data');
        $token = $data['token'];
        //code

        $this->json('post', 'api/kendaraan/', [], ['Accept' => 'application/json', 'Authorization' => "Bearer " .  $token,])->assertStatus(400);

        //endcode
        User::where('username', 'sampel123')->delete();
    }

    public function testSuccessCreateKendaraan()
    {
        User::create([
            "name" => "SampelUnique270501",
            "username" => "sampel123",
            "email" => "sampel123@test.com",
            "password" => bcrypt("sampel123"),
        ]);


        $loginData = ['username' => 'sampel123', 'password' => 'sampel123'];

        $data = $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])->json('data');
        $token = $data['token'];
        //code

        $kendaraan = [
            'tahun_keluaran' => 2002,
            'warna' => "rahasia270501",
            'harga' => 200000000,
            'mobil' => [
                'mesin' => "Dino",
                'kapasitas_penumpang' => 4,
                'tipe' => "Flamming",
            ],
            'stok' => true,
        ];
        // [
        //     'tahun_keluaran' => 2002,
        //     'warna' => "rahasia270501",
        //     'harga' => 1500000,
        //     'motor' => [
        //         'mesin' => "Rex",
        //         'tipe_suspensi' => "Sunny",
        //         'tipe_transmisi' => "Matic",
        //     ],
        //     'stok' => true,
        // ]

        $this->json('post', 'api/kendaraan/', $kendaraan, ['Accept' => 'application/json', 'Authorization' => "Bearer " .  $token,])->assertStatus(200);

        //endcode
        Kendaraan::where('warna', 'rahasia270501')->delete();
        User::where('username', 'sampel123')->delete();
    }

    public function testFailedUpdateKendaraan()
    {
        User::create([
            "name" => "SampelUnique270501",
            "username" => "sampel123",
            "email" => "sampel123@test.com",
            "password" => bcrypt("sampel123"),
        ]);

        $loginData = ['username' => 'sampel123', 'password' => 'sampel123'];

        $data = $this->json('POST', 'api/login/', $loginData, ['Accept' => 'application/json'])->json('data');
        $token = $data['token'];
        //code

        $kendaraan = [
            'tahun_keluaran' => 2002,
            'warna' => "rahasia270501",
            'harga' => 500000000,
            'mobil' => [
                'mesin' => "Dino",
                'kapasitas_penumpang' => 4,
                'tipe' => "Flamming",
            ],
            'stok' => true,
        ];
        // [
        //     'tahun_keluaran' => 2002,
        //     'warna' => "rahasia270501",
        //     'harga' => 1500000,
        //     'motor' => [
        //         'mesin' => "Rex",
        //         'tipe_suspensi' => "Sunny",
        //         'tipe_transmisi' => "Matic",
        //     ],
        //     'stok' => true,
        // ]

        $this->json('put', 'api/kendaraan/_id', $kendaraan, ['Accept' => 'application/json', 'Authorization' => "Bearer " .  $token,])->assertStatus(400);

        //endcode
        User::where('username', 'sampel123')->delete();
    }

    public function testSuccessUpdateKendaraan()
    {
        User::create([
            "name" => "SampelUnique270501",
            "username" => "sampel123",
            "email" => "sampel123@test.com",
            "password" => bcrypt("sampel123"),
        ]);

        $kendaraan = Kendaraan::create([
            [
                'tahun_keluaran' => 2002,
                'warna' => "rahasia270501",
                'harga' => 200000000,
                'mobil' => [
                    'mesin' => "Dino",
                    'kapasitas_penumpang' => 4,
                    'tipe' => "Flamming",
                ],
                'stok' => true,
            ],
            // [
            //     'tahun_keluaran' => 2002,
            //     'warna' => "rahasia270501",
            //     'harga' => 1500000,
            //     'motor' => [
            //         'mesin' => "Rex",
            //         'tipe_suspensi' => "Sunny",
            //         'tipe_transmisi' => "Matic",
            //     ],
            //     'stok' => true,
            // ]
        ]);
        $id = $kendaraan->id;


        $loginData = ['username' => 'sampel123', 'password' => 'sampel123'];

        $data = $this->json('POST', 'api/login/', $loginData, ['Accept' => 'application/json'])->json('data');
        $token = $data['token'];
        //code

        $kendaraan = [
            'tahun_keluaran' => 2002,
            'warna' => "rahasia270501",
            'harga' => 500000000,
            'mobil' => [
                'mesin' => "Dino",
                'kapasitas_penumpang' => 4,
                'tipe' => "Flamming",
            ],
            'stok' => true,
        ];
        // [
        //     'tahun_keluaran' => 2002,
        //     'warna' => "rahasia270501",
        //     'harga' => 1500000,
        //     'motor' => [
        //         'mesin' => "Rex",
        //         'tipe_suspensi' => "Sunny",
        //         'tipe_transmisi' => "Matic",
        //     ],
        //     'stok' => true,
        // ]

        $this->json('put', 'api/kendaraan/' . $id, $kendaraan, ['Accept' => 'application/json', 'Authorization' => "Bearer " .  $token,])->assertStatus(200);

        //endcode
        Kendaraan::where('warna', 'rahasia270501')->delete();
        User::where('username', 'sampel123')->delete();
    }

    public function testFailedDeleteKendaraan()
    {
        User::create([
            "name" => "SampelUnique270501",
            "username" => "sampel123",
            "email" => "sampel123@test.com",
            "password" => bcrypt("sampel123"),
        ]);

        $loginData = ['username' => 'sampel123', 'password' => 'sampel123'];

        $data = $this->json('POST', 'api/login/', $loginData, ['Accept' => 'application/json'])->json('data');
        $token = $data['token'];
        //code

        $this->json('delete', 'api/kendaraan/_id', [], ['Accept' => 'application/json', 'Authorization' => "Bearer " .  $token,])->assertStatus(400);

        //endcode
        User::where('username', 'sampel123')->delete();
    }

    public function testSuccessDeleteKendaraan()
    {
        User::create([
            "name" => "SampelUnique270501",
            "username" => "sampel123",
            "email" => "sampel123@test.com",
            "password" => bcrypt("sampel123"),
        ]);

        $loginData = ['username' => 'sampel123', 'password' => 'sampel123'];

        $data = $this->json('POST', 'api/login/', $loginData, ['Accept' => 'application/json'])->json('data');
        $token = $data['token'];
        //code

        $kendaraan = Kendaraan::create([
            [
                'tahun_keluaran' => 2002,
                'warna' => "rahasia270501",
                'harga' => 200000000,
                'mobil' => [
                    'mesin' => "Dino",
                    'kapasitas_penumpang' => 4,
                    'tipe' => "Flamming",
                ],
                'stok' => true,
            ],
            // [
            //     'tahun_keluaran' => 2002,
            //     'warna' => "rahasia270501",
            //     'harga' => 1500000,
            //     'motor' => [
            //         'mesin' => "Rex",
            //         'tipe_suspensi' => "Sunny",
            //         'tipe_transmisi' => "Matic",
            //     ],
            //     'stok' => true,
            // ]
        ]);
        $id = $kendaraan->id;

        $this->json('delete', 'api/kendaraan/' . $id, [], ['Accept' => 'application/json', 'Authorization' => "Bearer " .  $token,])->assertStatus(200);

        //endcode
        User::where('username', 'sampel123')->delete();
    }
}
