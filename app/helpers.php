<?php

if (!function_exists('responseData')) {
    function responseData($data, $code = 200, $success = true)
    {
        $response = [
            "status" => $code,
            "message" => ($success == true) ? "Success" : "Error",
            "data" => $data
        ];
        return $response;
    }
}
