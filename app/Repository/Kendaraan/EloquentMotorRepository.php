<?php

namespace App\Repository\Kendaraan;

use App\Repository\Kendaraan\EloquentKendaraanRepository;
use Illuminate\Http\Request;
use App\Models\Kendaraan;

class EloquentMotorRepository extends EloquentKendaraanRepository
{
    public function createKendaraan(Request $request)
    {
        $kendaraan = Kendaraan::create([
            'tahun_keluaran' => $request->tahun_keluaran,
            'warna' => $request->warna,
            'harga' => $request->harga,
            'motor' => [
                'mesin' => $request->motor['mesin'],
                'tipe_suspensi' => $request->motor['tipe_suspensi'],
                'tipe_transmisi' => $request->motor['tipe_transmisi'],
            ],
            'stok' => isset($request->stok) ? $request->stok : true,
        ]);
        if ($kendaraan != null) {
            return $kendaraan;
        }
        return null;
    }

    public function updateKendaraan(Request $request, $id)
    {
        $kendaraan = Kendaraan::find($id);
        $data = $kendaraan->toArray();
        if (array_key_exists('mobil', $data)) {
            $kendaraan->unset('mobil');
        }
        if ($kendaraan != null) {
            $kendaraan->update([
                'tahun_keluaran' => $request->tahun_keluaran,
                'warna' => $request->warna,
                'harga' => $request->harga,
                'motor' => [
                    'mesin' => $request->motor['mesin'],
                    'tipe_suspensi' => $request->motor['tipe_suspensi'],
                    'tipe_transmisi' => $request->motor['tipe_transmisi'],
                ],
                'stok' => isset($request->stok) ? $request->stok : true,
            ]);
            return $kendaraan;
        }
        return null;
    }
}
