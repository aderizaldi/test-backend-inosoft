<?php

namespace App\Repository\Kendaraan;

use App\Repository\Kendaraan\KendaraanRepository;
use Illuminate\Http\Request;
use App\Models\Kendaraan;

class EloquentKendaraanRepository implements KendaraanRepository
{
    public function createKendaraan(Request $request)
    {
        $kendaraan = Kendaraan::create([
            'tahun_keluaran' => $request->tahun_keluaran,
            'warna' => $request->warna,
            'harga' => $request->harga,
            'stok' => isset($request->stok) ? $request->stok : true,
        ]);
        if ($kendaraan != null) {
            return $kendaraan;
        }
        return null;
    }

    public function getAllKendaraan()
    {
        $kendaraan = Kendaraan::all();
        if ($kendaraan != null) {
            return $kendaraan;
        }
        return null;
    }

    public function getAllMotor()
    {
        $kendaraan = Kendaraan::where('motor', 'exists', true);
        if ($kendaraan != null) {
            return $kendaraan->get();
        }
        return null;
    }

    public function getAllMobil()
    {
        $kendaraan = Kendaraan::where('mobil', 'exists', true);
        if ($kendaraan != null) {
            return $kendaraan->get();
        }
        return null;
    }

    public function getKendaraanById($id)
    {
        $kendaraan = Kendaraan::find($id);
        if ($kendaraan != null) {
            return $kendaraan;
        }
        return null;
    }

    public function updateKendaraan(Request $request, $id)
    {
        $kendaraan = Kendaraan::find($id);
        if ($kendaraan != null) {
            $kendaraan->update([
                'tahun_keluaran' => $request->tahun_keluaran,
                'warna' => $request->warna,
                'harga' => $request->harga,
                'stok' => isset($request->stok) ? $request->stok : true,
            ]);
            return $kendaraan;
        }
        return null;
    }

    public function deleteKendaraan($id)
    {
        $kendaraan = Kendaraan::find($id);
        if ($kendaraan != null) {
            $kendaraan->delete();
            return $id;
        }
        return null;
    }

    public function buyKendaraan($id)
    {
        $kendaraan = Kendaraan::find($id);
        $data = $kendaraan->toArray();
        if ($data["stok"] == false) {
            return null;
        }
        if ($kendaraan != null) {
            $kendaraan->update([
                'stok' => false,
            ]);
            return $kendaraan;
        }
        return null;
    }

    public function getMobil()
    {
        $kendaraan = Kendaraan::where('stok', true)->where('mobil', 'exists', true);
        if ($kendaraan != null) {
            return $kendaraan->get();
        }
        return null;
    }
    public function getMotor()
    {
        $kendaraan = Kendaraan::where('stok', true)->where('motor', 'exists', true);
        if ($kendaraan != null) {
            return $kendaraan->get();
        }
        return null;
    }
    public function getKendaraan()
    {

        $kendaraan = Kendaraan::where('stok', true)->get();
        if ($kendaraan != null) {
            return $kendaraan;
        }
        return null;
    }
    public function countAllKendaraan(): int
    {
        $kendaraan = Kendaraan::all();
        $count = $kendaraan->count();
        if ($kendaraan != null) {
            return $count;
        }
        return 0;
    }
    public function countKendaraan(): int
    {
        $kendaraan = Kendaraan::where('stok', true);
        $count = $kendaraan->count();
        if ($kendaraan != null) {
            return $count;
        }
        return 0;
    }
    public function countMobil(): int
    {
        $kendaraan = Kendaraan::where('stok', true)->where('mobil', 'exists', true);
        $count = $kendaraan->count();
        if ($kendaraan != null) {
            return $count;
        }
        return 0;
    }
    public function countMotor(): int
    {
        $kendaraan = Kendaraan::where('stok', true)->where('motor', 'exists', true);
        $count = $kendaraan->count();
        if ($kendaraan != null) {
            return $count;
        }
        return 0;
    }
    public function countKendaraanTerjual(): int
    {
        $kendaraan = Kendaraan::where('stok', false);
        $count = $kendaraan->count();
        if ($kendaraan != null) {
            return $count;
        }
        return 0;
    }
    public function countAllMotor(): int
    {
        $kendaraan = Kendaraan::where('motor', 'exists', true);
        $count = $kendaraan->count();
        if ($kendaraan != null) {
            return $count;
        }
        return 0;
    }
    public function countAllMobil(): int
    {
        $kendaraan = Kendaraan::where('mobil', 'exists', true);
        $count = $kendaraan->count();
        if ($kendaraan != null) {
            return $count;
        }
        return 0;
    }
    public function countMobilTerjual(): int
    {
        $kendaraan = Kendaraan::where('stok', false)->where('mobil', 'exists', true);;
        $count = $kendaraan->count();
        if ($kendaraan != null) {
            return $count;
        }
        return 0;
    }
    public function countMotorTerjual(): int
    {
        $kendaraan = Kendaraan::where('stok', false)->where('motor', 'exists', true);;
        $count = $kendaraan->count();
        if ($kendaraan != null) {
            return $count;
        }
        return 0;
    }

    public function getMobilTerjual()
    {
        $kendaraan = Kendaraan::where('stok', false)->where('mobil', 'exists', true);
        if ($kendaraan != null) {
            return $kendaraan->get();
        }
        return null;
    }
    public function getMotorTerjual()
    {
        $kendaraan = Kendaraan::where('stok', false)->where('motor', 'exists', true);
        if ($kendaraan != null) {
            return $kendaraan->get();
        }
        return null;
    }
    public function getKendaraanTerjual()
    {
        $kendaraan = Kendaraan::where('stok', false);
        if ($kendaraan != null) {
            return $kendaraan->get();
        }
        return null;
    }
}
