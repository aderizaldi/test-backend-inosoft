<?php

namespace App\Repository\Kendaraan;

use App\Repository\Kendaraan\EloquentKendaraanRepository;
use Illuminate\Http\Request;
use App\Models\Kendaraan;

class EloquentMobilRepository extends EloquentKendaraanRepository
{
    public function createKendaraan(Request $request)
    {
        $kendaraan = Kendaraan::create([
            'tahun_keluaran' => $request->tahun_keluaran,
            'warna' => $request->warna,
            'harga' => $request->harga,
            'mobil' => [
                'mesin' => $request->mobil['mesin'],
                'kapasitas_penumpang' => $request->mobil['kapasitas_penumpang'],
                'tipe' => $request->mobil['tipe'],
            ],
            'stok' => isset($request->stok) ? $request->stok : true,
        ]);
        if ($kendaraan != null) {
            return $kendaraan;
        }
        return null;
    }

    public function updateKendaraan(Request $request, $id)
    {
        $kendaraan = Kendaraan::find($id);
        if ($kendaraan != null) {
            $data = (array) $kendaraan->get();
            if (array_key_exists('motor', $data)) {
                $kendaraan->unset('motor');
            }
            $kendaraan->update([
                'tahun_keluaran' => $request->tahun_keluaran,
                'warna' => $request->warna,
                'harga' => $request->Harga,
                'mobil' => [
                    'mesin' => $request->mobil['mesin'],
                    'kapasitas_penumpang' => $request->mobil['kapasitas_penumpang'],
                    'tipe' => $request->mobil['tipe'],
                ],
                'stok' => isset($request->stok) ? $request->stok : true,
            ]);
            return $kendaraan;
        }
        return null;
    }
}
