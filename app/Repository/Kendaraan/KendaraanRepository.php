<?php

namespace App\Repository\Kendaraan;

use Illuminate\Http\Request;

interface KendaraanRepository
{
    public function createKendaraan(Request $request);
    public function getAllKendaraan();
    public function getAllMotor();
    public function getAllMobil();
    public function getKendaraanById($id);
    public function updateKendaraan(Request $request, $id);
    public function deleteKendaraan($id);
    public function buyKendaraan($id);
    public function getMobil();
    public function getMotor();
    public function getKendaraan();
    public function countAllKendaraan(): int;
    public function countAllMobil(): int;
    public function countAllMotor(): int;
    public function countKendaraanTerjual(): int;
    public function countKendaraan(): int;
    public function countMobil(): int;
    public function countMotor(): int;
    public function countMobilTerjual(): int;
    public function countMotorTerjual(): int;
    public function getMobilTerjual();
    public function getMotorTerjual();
    public function getKendaraanTerjual();
}
