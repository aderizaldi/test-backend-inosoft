<?php

namespace App\Http\Controllers;

use App\Repository\Kendaraan\EloquentKendaraanRepository;
use App\Repository\Kendaraan\EloquentMotorRepository;
use App\Repository\Kendaraan\EloquentMobilRepository;
use Illuminate\Http\Request;
use App\Models\Kendaraan;
use Illuminate\Support\Facades\Validator;

class KendaraanController extends Controller
{
    protected $eloquentKendaraan;
    protected $eloquentMotor;
    protected $eloquentMobil;
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->eloquentKendaraan = new EloquentKendaraanRepository;
        $this->eloquentMobil = new EloquentMobilRepository;
        $this->eloquentMotor = new EloquentMotorRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kendaraan = $this->eloquentKendaraan->getAllKendaraan();
        $countAll = $this->eloquentKendaraan->countAllKendaraan();
        $countTerjual = $this->eloquentKendaraan->countKendaraanTerjual();
        $countStok = $this->eloquentKendaraan->countKendaraan();
        if (!is_null($kendaraan)) {
            return response()->json(responseData([
                "stok" => $countStok,
                "terjual" => $countTerjual,
                "total" => $countAll,
                "data" => $kendaraan,
            ]));
        }
        return response()->json(responseData([
            "stok" => $countStok,
            "terjual" => $countTerjual,
            "total" => $countAll,
            "data" => [],
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tahun_keluaran' => ['required'],
            'warna' => ['required'],
            'harga' => ['required'],
        ]);
        if ($validator->fails()) {
            return response()->json(responseData(["messages" => $validator->errors()], 400, false), 400);
        }

        if (isset($request->motor) && isset($request->mobil)) {
            return response("Gagal menambah data!");
        } else if (isset($request->motor)) {
            $kendaraan = $this->eloquentMotor->createKendaraan($request);
        } else if (isset($request->mobil)) {
            $kendaraan = $this->eloquentMobil->createKendaraan($request);
        } else {
            $kendaraan = $this->eloquentKendaraan->createKendaraan($request);
        }

        if (!is_null($kendaraan)) {
            return response()->json(responseData(['message' => "Berhasil menambah data"]));
        }
        return response()->json(responseData(['message' => "Gagal menambah data"], 400, false), 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kendaraan  $kendaraan
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kendaraan = $this->eloquentKendaraan->getKendaraanById($id);
        if (!is_null($kendaraan)) {
            return response()->json(responseData($kendaraan));
        }
        return response()->json(responseData(['message' => "Data dengan id " . $id . " tidak ditemukan"], 400, false), 400);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kendaraan  $kendaraan
     * @return \Illuminate\Http\Response
     */
    public function edit(Kendaraan $kendaraan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kendaraan  $kendaraan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (isset($request->motor) && isset($request->mobil)) {
            return response("Gagal update data!");
        } else if (isset($request->motor)) {
            $kendaraan = $this->eloquentMotor->updateKendaraan($request, $id);
        } else if (isset($request->mobil)) {
            $kendaraan = $this->eloquentMobil->updateKendaraan($request, $id);
        } else {
            $kendaraan = $this->eloquentKendaraan->updateKendaraan($request, $id);
        }
        if (!is_null($kendaraan)) {
            return response()->json(responseData(['message' => "Berhasil mengubah data"]));
        }
        return response()->json(responseData(['message' => "Gagal megubah data"], 400, false), 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kendaraan  $kendaraan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kendaraan = $this->eloquentKendaraan->deleteKendaraan($id);
        if (!is_null($kendaraan)) {
            return response()->json(responseData(['message' => "Data dengan id " . $id . " berhasil dihapus"]));
        }
        return response()->json(responseData(['message' => "Data dengan id " . $id . " tidak ditemukan"], 400, false), 400);
    }

    public function allMotor()
    {
        $kendaraan = $this->eloquentKendaraan->getAllMotor();
        $countAll = $this->eloquentKendaraan->countAllMotor();
        $countTerjual = $this->eloquentKendaraan->countMotorTerjual();
        $countStok = $this->eloquentKendaraan->countMotor();
        if (!is_null($kendaraan)) {
            return response()->json(responseData([
                "stok" => $countStok,
                "terjual" => $countTerjual,
                "total" => $countAll,
                "data" => $kendaraan,
            ]));
        }
        return response()->json(responseData([
            "stok" => $countStok,
            "terjual" => $countTerjual,
            "total" => $countAll,
            "data" => [],
        ]));
    }
    public function allMobil()
    {
        $kendaraan = $this->eloquentKendaraan->getAllMobil();
        $countAll = $this->eloquentKendaraan->countAllMobil();
        $countTerjual = $this->eloquentKendaraan->countMobilTerjual();
        $countStok = $this->eloquentKendaraan->countMobil();
        if (!is_null($kendaraan)) {
            return response()->json(responseData([
                "stok" => $countStok,
                "terjual" => $countTerjual,
                "total" => $countAll,
                "data" => $kendaraan,
            ]));
        }
        return response()->json(responseData([
            "stok" => $countStok,
            "terjual" => $countTerjual,
            "total" => $countAll,
            "data" => [],
        ]));
    }
    public function buyKendaraan($id)
    {
        $kendaraan = $this->eloquentKendaraan->buyKendaraan($id);
        if (!is_null($kendaraan)) {
            return response()->json(responseData(["message" => "Berhasi membeli kendaraan", "data" => $kendaraan]));
        }
        return response()->json(responseData(['message' => "Gagal membeli kendaraan"], 400, false), 400);
    }
}
