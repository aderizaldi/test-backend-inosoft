<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => ['required'],
            'password' => ['required'],
        ]);
        if ($validator->fails()) {
            return response()->json(responseData(["messages" => $validator->errors()], 400, false), 400);
        }

        if (!$token = auth()->attempt($request->only('username', 'password'))) {
            return response()->json(responseData(['message' => "Username atau password salah"], 401, false), 401);
        }

        return response()->json(responseData(["massage" => "Berhasil login", "token" => $token]));
    }
}
