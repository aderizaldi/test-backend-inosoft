<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;


class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // request()->validate('post', [
        //     'name' => ['required', 'string'],
        //     'username' => ['required', 'alpha_num', 'min:3', 'max:24', 'unique:users,username'],
        //     'email' => ['required', 'email', 'unique:users,uemail'],
        //     'password' => ['required', 'min:6'],
        // ]);
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'username' => ['required', 'alpha_num', 'min:3', 'max:24', 'unique:users,username'],
            'email' => ['required', 'email', 'unique:users,email'],
            'password' => ['required', 'min:6'],
        ]);
        if ($validator->fails()) {
            return response()->json(responseData(["messages" => $validator->errors()], 400, false), 400);
        }

        User::create([
            'name' => request('name'),
            'username' => request('username'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
        ]);

        return response()->json(responseData(['message' => "Register berhasil"]));
    }
}
